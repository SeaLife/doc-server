#!/bin/bash

DOC_SERVER_PID="application.pid"

echo "$0 ${@}"

PORT=
PROFILES=
DEBUG_PORT=

while getopts 'p:P:d:' opt
do
    case "$opt" in
        p )
            PORT="$OPTARG"; shift 2
            ;;
        P )
            PROFILES="$OPTARG"; shift 2
            ;;
        d )
            DEBUG_PORT="$OPTARG"; shift 2
            ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

echo $PORT
echo $DEBUG_PORT
echo $PROFILES

DEBUG_OPTS=""
if ! [ -z "$DEBUG_PORT" ]; then
    DEBUG_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=${DEBUG_PORT}"
fi

JAVA_OPTS="-Dserver.port=${PORT} -Dspring.profiles=${PROFILES} ${DEBUG_OPTS}"




# Find JAVA_HOME if not already set
if [ -z "${JAVA_HOME}" ]
 then
    JAVA_HOME=$(dirname $(dirname $(readlink -f $(which java))))
fi



# check the JAVA_HOME path
if [ ! -e ${JAVA_HOME}/bin/java ]
 then
    echo "No java executable found at JAVA_HOME=$JAVA_HOME"
    exit 1
 else
    echo "Found java binary at ${JAVA_HOME}. Continue..."
fi




# check if the pid file exists
check_pid_file(){
 if [ ! -f ${DOC_SERVER_PID} ]
  then
    echo "Doc server is not running."
    exit 1
 fi
}


# only check doc server process
is_running(){
 if ps -p "$(cat ${DOC_SERVER_PID})" > /dev/null
 then
     return 0
 else
     return 1
 fi
}



# start the application
if is_running
then
  echo "Process "$(cat ${DOC_SERVER_PID})" already running"
  exit 1
fi

echo "${JAVA_HOME}/bin/java ${JAVA_OPTS} $2 -jar *.jar"
${JAVA_HOME}/bin/java ${JAVA_OPTS} -jar *.jar



