package com.gitlab.sf.projects.docserver.impl;

import lombok.Data;

import java.io.File;

/**
 * Represents artifact information based on the file path.
 *
 * Example: /home/user/doc-server/my-group/my-artifact/1.0-SNAPSHOT/index.html
 * <ul>
 *     <li>groupId = my-group</li>
 *     <li>artifactId = my-artifact</li>
 *     <li>version = 1.0-SNAPSHOT</li>
 *     <li>fileName = index.html</li>
 * </ul>
 *
 */
@Data
final class ArtifactItemInfo {

    public static ArtifactItemInfo fromFile(File file) {
        final File versionFile = file.getParentFile();
        if(versionFile == null) {
            throw new IllegalArgumentException("Need directory representing version");
        }
        final File artifactIdFile = versionFile.getParentFile();
        if(artifactIdFile == null) {
            throw new IllegalArgumentException("Need directory representing artifactId");
        }
        final File groupIdFile = artifactIdFile.getParentFile();
        if(groupIdFile == null) {
            throw new IllegalArgumentException("Need directory representing groupId");
        }
        return new ArtifactItemInfo(file, groupIdFile.getName(), artifactIdFile.getName(), versionFile.getName());
    }


    private final File file;
    private final String groupId;
    private final String artifactId;
    private final String version;

    public ArtifactItemInfo(File file, String groupId, String artifactId, String version) {
        this.file = file;
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.version = version;
    }

    /**
     * @return constructed path with form /groupId/artifactId/version/fileName
     */
    String getPath() {
        return File.separator + groupId
                + File.separator + artifactId
                + File.separator + version
                + File.separator + file.getName();
    }
}
