package com.gitlab.sf.projects.docserver.impl;

import lombok.Getter;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Getter
class Group {
    private final String groupId;

    private final Map<String, Artifact> artifactMap = new ConcurrentHashMap<>();

    public Group(String groupId) {
        Assert.hasText(groupId);

        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;

        Group group = (Group) o;

        return groupId.equals(group.groupId);

    }

    @Override
    public int hashCode() {
        return groupId.hashCode();
    }

    public Artifact getArtifact(String artifactId) {
        return artifactMap.get(artifactId);
    }

    public Collection<Artifact> getArtifacts() {
        return artifactMap.values().parallelStream().sorted(Artifact::compareTo).collect(Collectors.toList());
    }

    public boolean hasArtifact(String artifactId) {
        return artifactMap.containsKey(artifactId);
    }

    public void addArtifact(Artifact artifact) {
        this.artifactMap.put(artifact.getArtifactId(), artifact);
    }

    public void remove(ArtifactItemInfo artifactItemInfo) {
        Artifact artifact = (Artifact)artifactMap.get(artifactItemInfo.getArtifactId());
        artifact.removeVersion(artifactItemInfo.getVersion());
        if(artifact.getVersions().isEmpty()) {
            artifactMap.remove(artifact.getArtifactId());
        }
    }

    public static int compareTo(Group a, Group b)
    {
        return a.getGroupId().compareTo(b.getGroupId());
    }

    public void removeVersion(ArtifactVersion artifactVersion) {
        Artifact artifact = (Artifact)artifactMap.get(artifactVersion.getArtifactId());
        artifact.removeVersion(artifactVersion.getVersion());
        if(artifact.getVersions().isEmpty()) {
            artifactMap.remove(artifact.getArtifactId());
        }
    }
}
