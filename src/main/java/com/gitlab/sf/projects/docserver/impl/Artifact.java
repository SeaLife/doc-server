package com.gitlab.sf.projects.docserver.impl;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.*;

/**
 * Defines a documentation artifact model
 */
@Getter
@Setter
final class Artifact {
    private static final String PATH_SEP = ":";

    static String print(Artifact artifact) {
        return artifact.getGroupId() + PATH_SEP + artifact.getArtifactId();
    }

    static String print(ArtifactVersion artifactVersion) {
        return print(artifactVersion.getArtifact()) + PATH_SEP + artifactVersion.getVersion();
    }


    private String artifactId;
    private Group group;

    private Set<ArtifactVersion> versions = Collections.synchronizedSortedSet(new TreeSet<>(ArtifactVersion::compareTo));

    public Artifact(Group group, String artifactId) {
        Assert.notNull(group);
        Assert.hasText(artifactId);

        this.group = group;
        this.artifactId = artifactId;
    }

    public Collection<ArtifactVersion> getVersions() {
        return Collections.unmodifiableCollection(versions);
    }

    void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return Artifact.print(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Artifact)) return false;

        Artifact artifact = (Artifact) o;

        if (!artifactId.equals(artifact.artifactId)) return false;
        return group.equals(artifact.group);

    }

    @Override
    public int hashCode() {
        int result = artifactId.hashCode();
        result = 31 * result + group.hashCode();
        return result;
    }

    public ArtifactVersion getLatestReleaseVersion()
    {
        final Iterator<ArtifactVersion> versionIterator = getVersions().iterator();
        ArtifactVersion result = null;
        while (versionIterator.hasNext()) {
            ArtifactVersion version = versionIterator.next();
            if(version.isRelease()) {
                result = version;
                break;
            }
        }
        return result;
    }

    public String getGroupId() {
        return group.getGroupId();
    }

    public void removeVersion(String version) {
        versions.removeIf(artifactVersion -> artifactVersion.getVersion().equals(version));
    }

    public void addVersion(ArtifactItemInfo artifactItemInfo) {
        versions.add(new ArtifactVersion(this, artifactItemInfo));
    }

    public static int compareTo(Artifact artifact, Artifact b)
    {
        return artifact.getArtifactId().compareTo(b.getArtifactId());
    }
}
