package com.gitlab.sf.projects.docserver.impl;


import com.gitlab.sf.projects.docserver.DocServerProperties;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.Optional;

/**
 * Responsible for serving artifact documentations by their hash code.
 */
@SuppressWarnings("UnusedReturnValue")
@Controller
public class DocController {

    private final String VIEW_NAME = "doc";

    private final DocServerProperties docServerProperties;
    private final Cache cache;

    public DocController(DocServerProperties docServerProperties, Cache cache) {
        this.docServerProperties = docServerProperties;
        this.cache = cache;
    }

    /**
     * Returns latest doc view presentation for groupId/artifactId
     */
    @RequestMapping("/doc/groupId/{groupId}/artifactId/{artifactId}/get")
    public String getDoc(@PathVariable("groupId") String groupId,
                         @PathVariable("artifactId") String artifactId,
                         Model model) {
        Artifact artifact = getArtifact(groupId, artifactId);
        ArtifactVersion artifactVersion = artifact.getLatestReleaseVersion();
        if(artifactVersion == null) {
            artifactVersion = artifact.getVersions().iterator().next();
        }
        updateModel(model, artifact, artifactVersion);
        return VIEW_NAME;
    }

    /**
     * Returns requested version of doc view presentation for groupId/artifactId
     */
    @RequestMapping(path = "/doc/groupId/{groupId}/artifactId/{artifactId}/version/{version}/get",
            method = RequestMethod.GET)
    public String getDoc(@PathVariable("groupId") String groupId,
                         @PathVariable("artifactId") String artifactId,
                         @PathVariable("version") String version,
                         Model model) {
        Artifact artifact = getArtifact(groupId, artifactId);
        Optional<ArtifactVersion> artifactVersionOptional = artifact.getVersions().stream()
                .filter(item -> item.getVersion().equals(version)).findFirst();

        if(artifactVersionOptional.isPresent()) {
            ArtifactVersion artifactVersion = artifactVersionOptional.get();
            updateModel(model, artifact, artifactVersion);

        } else {
            getDoc(groupId, artifactId, model);
        }

        return VIEW_NAME;
    }


    /**
     * Returns requested version of doc view presentation for groupId/artifactId
     */
    @RequestMapping(path = "/doc/groupId/{groupId}/artifactId/{artifactId}/version/{version}/get-pdf",
            method = RequestMethod.GET, produces = "application/pdf")
    @ResponseBody
    public FileSystemResource getDocPdf(@PathVariable("groupId") String groupId,
                                        @PathVariable("artifactId") String artifactId,
                                        @PathVariable("version") String version,
                                        Model model) {
        Artifact artifact = getArtifact(groupId, artifactId);
        FileSystemResource result = null;
        if(artifact != null) {
            Optional<ArtifactVersion> artifactVersionOptional = artifact.getVersions().stream()
                    .filter(item -> item.getVersion().equals(version)).findFirst();

            if (artifactVersionOptional.isPresent()) {
                ArtifactVersion artifactVersion = artifactVersionOptional.get();
                File file = artifactVersion.getPdfFile();
                if (file != null) {
                    result = new FileSystemResource(file);
                }
            }
        }

        return result;
    }

    private Artifact getArtifact(String groupId, String artifactId) {
        Group group = cache.getGroup(groupId);
        return group.getArtifact(artifactId);
    }

    private void updateModel(Model model, Artifact artifact, ArtifactVersion artifactVersion) {
        Group group = cache.getGroup(artifact.getGroupId());
        model.addAttribute("artifacts", group.getArtifacts());
        model.addAttribute("artifact", artifact);
        model.addAttribute("artifactVersion", artifactVersion);
    }

    @ModelAttribute("title")
    public String getTitle() {
        return docServerProperties.getDocTitle();
    }
}
