package com.gitlab.sf.projects.docserver.impl;

import com.gitlab.sf.projects.docserver.DocServerProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.filters.AcceptOnceFileListFilter;
import org.springframework.integration.file.filters.CompositeFileListFilter;
import org.springframework.integration.file.filters.SimplePatternFileListFilter;
import org.springframework.integration.transformer.PayloadTypeConvertingTransformer;

import java.io.File;
import java.util.Arrays;


/**
 * Configure doc server file system integration.
 *
 */
@Configuration
@ComponentScan
public class ArtifactItemScanConfig {

    @Autowired
    private DocServerProperties docServerProperties;


    /**
     * Filter index.html once
     *
     * @return the file inbound channel message source
     */
    @Bean
    @InboundChannelAdapter(value = "fileInputChannel", poller = @Poller(fixedDelay = "1000"))
    public MessageSource<File> fileReadingMessageSource(CompositeFileListFilter compositeFileListFilter) {
        FileReadingMessageSource source = new FileReadingMessageSource();
        source.setDirectory(new File(docServerProperties.getDocPath()));
        source.setWatchEvents(FileReadingMessageSource.WatchEventType.CREATE);
        source.setFilter(compositeFileListFilter);
        source.setUseWatchService(true);
        return source;
    }


    /**
     * @return composite file list accept once filter only accepting files named index.html
     */
    @Bean
    public CompositeFileListFilter compositeFileListFilter() {
        SimplePatternFileListFilter indexHtmlFilter = new SimplePatternFileListFilter("index.html");
        AcceptOnceFileListFilter<File> acceptOnceFileListFilter = new AcceptOnceFileListFilter<>();
        CompositeFileListFilter result = new CompositeFileListFilter(
                Arrays.asList(indexHtmlFilter, acceptOnceFileListFilter));
        return result;
    }

    /**
     * @return transformer that converts a file into object of type {@link ArtifactItemInfo}
     *
     * @see SimpleCache
     */
    @Bean
    @Transformer(inputChannel = "fileInputChannel", outputChannel = "cacheInputChannel")
    public PayloadTypeConvertingTransformer<File, ArtifactItemInfo> artifactFileCreatedTransformer() {
        PayloadTypeConvertingTransformer<File, ArtifactItemInfo> result = new PayloadTypeConvertingTransformer<>();
        result.setConverter(ArtifactItemInfo::fromFile);
        return result;
    }

}
