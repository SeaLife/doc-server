#!/bin/bash

# Entry Script

echo "Running entry script":

echo -e "\tArguments: ${@}\n"

CMD="java ${@} -jar *.jar"

echo ${CMD}
exec ${CMD}
